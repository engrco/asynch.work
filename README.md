**Improvement is primarily about the obstacles to improvement that you remove.** 

DO NOT **squander** people's time, energy, ambition and good will with ***another*** meeting, ***another*** Zoom session, ***another*** email, ***another*** txt message, ***another***  posting on Slack or internal messaging channel.

Minimize communtes, minimize non-value-added activities ... drive all of your communication DIRECTLY to those things which are integral parts of the value created in workflow itself ... over-communicate, but do it through the work itself ... it varies with the work, but let the work speak -- for example, if you develop software, **directly** in code, in the code review through the [issues, issue boards and issue-tracking](https://docs.gitlab.com/ee/topics/gitlab_flow.html#issue-tracking-with-gitlab-flow) and the [merge/pull requests](https://docs.gitlab.com/ee/topics/gitlab_flow.html#mergepull-requests-with-gitlab-flow) which is requesite knowledge of doing the job.

Re-iterate the [semiotics](https://en.wikipedia.org/wiki/Semiotics) throughout everything, but especially the onboarding and training process to get colleagues to speak through the work so that they can express themselves in the realm they inhabit ... topic-appropriate signs, workflow practices, gestures, gists, semiotoics or basic language expression ... the pre-language semiotics are like martial arts practices ... the shared understandings that human teamwork depends upon in critical situations is not usually verbal ... think about how important eye-contact or head nods are for signalling, ie they are not improved by four-page instruction process with diagrams.

There's no good substitute for driving the communication in and through the work. Our ancestors vocalized minimally into order to hunt together in teams, to build civilization, to describe pain or the taste of food ... we should not forget the essentiality of this basic expression ... the best language is something that's understood across language or without language, but is a basic sign of discipline or competence appreciated by craftsmen and professionals *without any need for meetings, emails, messages ... or diplomatic language*. 

Every workflow has its **standard** workflow set of [semiotics](https://en.wikipedia.org/wiki/Semiotics) which participants KNOW ... they also KNOW when someone's struggling or not getting something right... these inherent signs or indicators don't benefit from another meeting, another Zoom session, another call/text, another email or another posting to a messaging channel ... the semiotic flow is not added; it inherent in the flow of the work ... *the other* **NOISE** *which is added is WASTE that causes MORE waste.* 

For example, the inherent standard communication semiotics are part of [the stardard workflow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) of version-controlled repositories of content that your team collaborates on -- there are already plenty of notifications ... *there's no reason to ADD other additional layers of meetings, emails, messages, postings to* **improve** *communication.*  ***FOCUS on the workflow*** ... and ONLY on the workflow!

Where is the value that is really ADDED? Do NOT use the serendipity of creative people held captive in a location, who are forced to make the best of a meeting or a commute ... examine where and how the flow could be designed for  more value to be optimally added. *DO BETTER!!!*  

**Improvement is about what you remove**  ... remove the OBSTACTLES of meeting-centric cultures ... free people's minds by REMOVING the obstacles to work ... make it possible for people collaborate **asynchronomously** ... according to THEIR schedule but yet meeting a project mgmt or shipping schedule, not because of an artificial meeting timetable ... regardless of timezone, around the globe ... asynch.WORK is about bathing the workflow in communication and information making work FLOW.
## Use D.O.W.N.T.I.M.E. To See Waste

Most human activity is WASTE ... it is waste that persists because it is never questioned. 

We finally have the communication methods and information technology that allow us to add massive amounts of value by SUBTRACTION ... as we remove all forms of waste, we can be assured that we will see a lot more waste. **Improvement is about what you remove.** 

Push the D.O.W.N.T.I.M.E. mnemonic device to help everyone remember that have a responsiblity to focus upon seeing waste and pointing it out ... so that waste can be acknowledged, discussed and REMOVED.

* **D** *Defects* or ANYTHING not demanded ... waste is any obstactle to quality
* **O** *Overpolishing* or ANY form of overprduction or overinspection is WASTE
* **W** *Waiting* or even latency indicates lack of disciplined concern for others 
* **N** *NOT* Listening, NOT using others ideas is arrogant, waste-perpetuation
* **T** *Transport*, especially of people, subtracts massive amounts of value
* **I** *Inventory* is always a burden, never an asset, that includes data
* **M** *Motion* to appear active is pure idiocy and fritters away resources
* **E** *Ego* or anything extra for show / signalling is worse than other waste

## Discipline Equals Freedom

Use the Plan-Do-Check-Act discipline inherent in tracking functionality of Issues and shared Issue Boards of something like [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) ... do not add another meeting, webinar, email, messaging channel, calendaring app, organizational tool or whizz-bang doo-dad -- instead USE distributed version-controlled content repositories as the basis to go back to for ALL communication.

## Apply S.M.A.R.T discipline in all issues

Plan work to be **Specific** and **Measureable**; err on the side of being slightly **Aggressive** but keep work upbeat, **Realistic** and **Time-constrained**, ie doable within one day.