---
title: Quality vs Quantity
subtitle: Why not BOTH?
date: 2021-05-01
tags: ["tool", "discipline"]
---


## The example of exercise and workouts

Quantity kind of has to come first ... you need ONE repititon in order to have any chance of making it a QUALITY repitition, up to your standards of perfection -- you will NEVER really get it to your standards of PURE perfection, but you can feel good about what is good about it or how it was better than you used to do or at least good enough to show that you are still alive and trying ... so, of course, we could say that only quality reps count ... but you still need the quantity of something. 

The key to getting going with any exercise or training program is really that FIRST rep that you do ... it might be the first rep that gets your workout started after you were manufacturing excuses about why you weren't going to workout today ... OR it might be that FIRST rep that you do, after you have kinda decided that you want to quit and just don't want to do any more.  You have to do that FIRST rep ... THAT first rep, after the excuses, is the one that matters.  Ten reps to show off in front of friends when you can do 100 do not matter AT ALL -- except maybe as part of the back-and-forth bs'ing you do with friends.

So quanity matters ... but ONLY that quantity of ONE rep after you've decide to quit ... quality matters in determining how rapidly you can workout and get to that point where you don't have more to give. You might need to add kettlebells to your yoga routine when bodyweight is insufficient, but quality might also be about holding a pose for a certain length of time.

## WHO or WHAT are you training for?

Extreme ownership is all about high agency ... completely owning a mission ... being willing AND able AND ready to respond immediately when called upon.  You sort of have to pick which hill to die on or where/how you are going to train to respond ... the actual mission will not be the same as the mission you train for, but there's an expression that is used for training for actual missions -- it's knowing as "fighting the LAST war."  It's rear-view mirror remininiscing about glory days -- a totally useless and hilarious cartoon, like Al Bundy slouching on the sofa, scratching his nuts, holding a beer and dreaming of when he scored four touchdowns in the big high school game -- THAT is why you don't train for the actual battle and it doesn't matter WHAT your experience is, e.g, "Well, my knowledge of fighting in [Gaudalcanal](https://en.wikipedia.org/wiki/Guadalcanal_campaign) tells me, that ... " If you say anything REMOTELY like that or relying on any of your experience as the reason why you don't need to train/prep like a motherfucker, then you are not a high agency person, regardless of what you imagine your experience is.

There's NO substitute for the training and preparation that you must do in the next hour, next day ... but in order to train effectively, you must train FUNCTIONALLY, with a clear, concrete idea of who/what you are training for ... because quality of execution comes down to ready, functional fitness for some sort of customer specification.  So WHO is this customer, ie it is likely YOU if you are dogfooding the stuff you will help others do?  What's the battle or war that you're going to fight for the customer ... and WHY do you need to be ready to fight this kind of fight, ie what does YOUR intelligence gathering tell you about the future OR are you listening to and being programmed by some politico or marketer with a narrative and an agenda that is about weaponizing you as a gadget or cannon fodder for THEIR war? 

## Summary

The QUANTITY is also ONE ... you only get ONE life ... the only rep that matters is the ONE that you do after you've decided you've had enough.

The QUALITY of your life depends upon your intelligence.  Are you developing your skills, your tools? What your specification for QUALITY execution?  OR  Are you being programmed, weaponized as per someone else's specification, as someone else's tool?